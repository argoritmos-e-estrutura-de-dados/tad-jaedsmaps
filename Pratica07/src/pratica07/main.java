package pratica07;

/** @author messiah */
public class main {
    public static void main(String[] args) throws Exception {
        System.out.println("Grafo 1:");
        JGraph jg_0 = new JGraph(6);        
        jg_0.insertEdge(1, 2, 8, 3);
        jg_0.insertEdge(1, 4, 5, 2);
        jg_0.insertEdge(1, 3, 4, 10);
        jg_0.insertEdge(2, 5, 9, 5);
        jg_0.insertEdge(3, 4, 1, 4);
        jg_0.insertEdge(3, 5, 5, 2);
        jg_0.insertEdge(4, 2, 3, 8);
        jg_0.insertEdge(4, 3, 5, 7);
        jg_0.insertEdge(4, 5, 2, 7);
                
        jg_0.print();
        
        JAEDsMaps jAEDsMaps_0 = new JAEDsMaps(jg_0);
        System.out.println("Caminho minímo: ");
        jAEDsMaps_0.searchWeight(1);
        jAEDsMaps_0.printDijkstra(1, 5);
                      
        System.out.println("\nGrafo 2: ");        
        JGraph jg_1 = new JGraph(6);
        jg_1.insertEdge(1, 2, 3, 3);
        jg_1.insertEdge(1, 4, 5, 5);
        jg_1.insertEdge(2, 3, 2, 6);
        jg_1.insertEdge(2, 4, 2, 2);
        jg_1.insertEdge(3, 5, 2, 2);
        jg_1.insertEdge(4, 2, 3, 1);
        jg_1.insertEdge(4, 3, 5, 4);
        jg_1.insertEdge(4, 5, 9, 6);
        jg_1.insertEdge(5, 1, 6, 3);
        jg_1.insertEdge(5, 3, 4, 7);        
        
        jg_1.print();
        
        JAEDsMaps jAEDsMaps_1 = new JAEDsMaps(jg_1);
        System.out.println("Caminho minímo: ");
        jAEDsMaps_1.searchWeight(4);
        jAEDsMaps_1.printDijkstra(4, 1);
        
    }
    
}