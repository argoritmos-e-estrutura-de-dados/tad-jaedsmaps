package pratica07;

public class JAEDsMaps {
    /** Armazena as distâncias relativas para cada vértice em cada iteração. */
    private int neighbors_nodes[];
    /** Armazena os nós vizinhos de cada nó do grafo da árvore final produzida. */
    private int relative_distance[];
    
    private JGraph jg;
    int n;
        
    public JAEDsMaps(JGraph jg){
        this.jg = jg;
        this.n = jg.getN();        
        this.relative_distance = new int[this.n];        
        this.neighbors_nodes = new int[this.n];
        
    }    
    
    public void searchWeight(int v0) throws Exception{
        /** Armazena os vértices já verificados.. */
        int verified_vertices[] = new int[this.n+1];
        
        for(int i=0; i<this.n; i++){
            verified_vertices[i+1] = i;
            relative_distance[i] = Integer.MAX_VALUE;
            neighbors_nodes[i] = -1;
        }
        relative_distance[v0] = 0;
       /** Contrói o heap de pesos minímos. */
        Heap jgraph_heap = new Heap(relative_distance, verified_vertices);
        jgraph_heap.build();
        
        /** Remove o vertice de valor minimo do heap até que este esteja vazio. */
        while(!jgraph_heap.isEmpty()){            
            int heap_min_value = jgraph_heap.removeMin();
            if(!this.jg.listEdgeisEmpty(heap_min_value)){
                /** adj recebe o primeiro valor da lista de arestas da matriz de adjacência. */
                JGraph.Edge adj = jg.firstListEdge(heap_min_value);
                /** Enquanto existir algum item na matriz de adjacência. */
                while(adj != null){
                    int v1 = adj.getV1();
                    /** Verifica se os pesos do primeiro item da lista da restas somados a 
                    distanciaRelativa do nó do ponto avaliado é menor que a atual distanciaRelativa
                    do nó vizinho; Caso sim, a distanciaRelativa do nó vizinho ao que está
                    sendo avaliado no momento será atualizada pelo valor dessa nova distancia 
                    avaliada até o pontoAvaliado. */
                    if(relative_distance[v1] > (relative_distance[heap_min_value] + adj.getWeight() + adj.getTime())){
                        neighbors_nodes[v1] = heap_min_value;
                        jgraph_heap.decreaseKey(v1, relative_distance[heap_min_value] + adj.getWeight() + adj.getTime());
                    }
                    adj = jg.nextEdge(heap_min_value);
                }
            }
        }      
    }
    public int antecessor (int v0) { 
        return this.neighbors_nodes[v0]; 
    }
    
    public void printDijkstra (int v0, int v1) {
    JGraph.Edge edge;
    // Se estiver na origem imprime ela
    if (v0 == v1) System.out.println (v0 + " -- Distancia: 0" + " - Tempo: 0");
    // se não existir antecessor não a como percorrer logo não existe o caminho entre os vertices requisitados
    else if (this.neighbors_nodes[v1] == -1)
      System.out.println ("Nao existe caminho de " +v0+ " ate " +v1);
    //Chama recursivamente com a origem e o antecessor imprimindo o caminho minimo entre os dois vertices
    else {
      printDijkstra (v0, this.neighbors_nodes[v1]);
      edge = jg.existEdge(antecessor(v1), v1);
      System.out.println (neighbors_nodes[v1] + "," + v1 + " -- Distancia:"  + edge.getWeight() + " - Tempo:" + edge.getTime());
    }    
  }
}
