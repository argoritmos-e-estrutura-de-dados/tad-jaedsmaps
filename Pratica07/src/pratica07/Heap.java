package pratica07;

/** @author messiah */
public class Heap {
    private int p[]; 
    private int n, priority_queue[];
    private int position[];    

  public Heap (int p[], int pq[]) { 
    this.p = p; 
    this.priority_queue = pq; 
    this.n = this.priority_queue.length-1;
    this.position = new int[this.n];
    
    for (int i = 0; i < this.n; i++) 
        this.position[i] = i+1;
  }

  public void remake(int left, int right) {
    int j = left * 2;
    int pq = this.priority_queue[left];
    while (j <= right) {
      if ((j < right) && (this.p[priority_queue[j]] > this.p[priority_queue[j + 1]])) 
          j++;
      if (this.p[pq] <= this.p[priority_queue[j]]) 
          break;
      this.priority_queue[left] = this.priority_queue[j]; 
      this.position[priority_queue[j]] = left; 
      left = j; 
      j = left * 2;
    }
    this.priority_queue[left] = pq; 
    this.position[pq] = left;
  }

  public void build () {
    int esq = n / 2 + 1;
    while (esq > 1) { 
        esq--; 
        this.remake(esq, this.n);
    }
  }
 
  public int removeMin () throws Exception {
    int minimo;
    if (this.n < 1) throw new Exception ("Erro: heap vazio");
    else {
      minimo = this.priority_queue[1]; 
      this.priority_queue[1] = this.priority_queue[this.n];
      this.position[priority_queue[this.n--]] = 1; 
      this.remake(1, this.n);
    }
    return minimo;
  }

  public void decreaseKey (int i, int chaveNova) throws Exception {
    i = this.position[i]; int x = priority_queue[i];
    if (chaveNova < 0)
      throw new Exception ("Erro: Chave com valor incorreto.");
    this.p[x] = chaveNova;
    while ((i > 1) && (this.p[x] <= this.p[priority_queue[i / 2]])) {
      this.priority_queue[i] = this.priority_queue[i / 2];
      this.position[priority_queue[i / 2]] = i; 
      i /= 2;
    }
    this.priority_queue[i] = x;
    this.position[x] = i;
  }

  boolean isEmpty () { 
      return this.n == 0; 
  }
    
  public void imprime () {
    for (int i = 1; i <= this.n; i++)
      System.out.print (this.p[priority_queue[i]] + " ");
    System.out.println ();
  }

}
